# Copyright 2012, 2014-201-2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_install

SUMMARY="A lightweight modular compositing manager"
DESCRIPTION="It aims to be efficient and responsive. It is currently written in C programming
language and based on XCB library client library. Any existing window manager can be used as long as
it implements properly EWMH and ICCCM specifications (or at least parts of it which are needed),
which is generally the case nowadays."

HOMEPAGE="http://projects.mini-dweeb.org/projects/${PN}"
DOWNLOADS="http://projects.mini-dweeb.org/attachments/download/114/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        x11-proto/xcb-proto[>=1.6]
        x11-proto/xorgproto
        virtual/pkg-config[>=0.9.0]
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/confuse
        dev-libs/libev
        x11-libs/libxcb[>=1.8]
        x11-libs/libxdg-basedir[>=1.0.0]
        x11-utils/xcb-util
        x11-utils/xcb-util-keysyms
        x11-utils/xcb-util-image
        x11-utils/xcb-util-renderutil
        x11-utils/xcb-util-wm
"

if ever at_least 0.3.4_p20140306 ; then
    DEPENDENCIES+="
        build+run:
            sys-apps/dbus
            x11-dri/libdrm
            x11-libs/libxkbcommon
            x11-utils/xcb-util-wm[>=0.4.0]
    "
fi

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc doxygen-doc' )

unagi_src_install() {
    default

    if option doc; then
        docinto html
        dodoc "${WORK}"/doc/api/html/*
    fi
}

