# Copyright 2017-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'livestreamer.exlib', which is:
#     Copyright 2014 Benedikt Morbach and 2016 Julian Ospald

require bash-completion zsh-completion
# Order matters unfortunately
if ever is_scm ; then
    require setup-py [ import=setuptools blacklist=2 test=pytest ]
    require github [ tag=${PV} ]
else
    require github [ tag=${PV} ]
    require setup-py [ import=setuptools blacklist=2 test=pytest ]
fi

SUMMARY="CLI for extracting streams from various websites to a video player"
DESCRIPTION="
Streamlink is a command-line utility that pipes video streams from various
services into a video player, such as VLC. The main purpose of Streamlink is
to allow the user to avoid buggy and CPU heavy flash plugins but still be able
to enjoy various streamed content. There is also an API available for
developers who want access to the video stream data.
Look at https://streamlink.github.io/plugin_matrix.html#plugin-matrix for a
list of plugins.
"
HOMEPAGE+=" https://streamlink.github.io/"

LICENCES="BSD-2 MIT"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/recommonmark[python_abis:*(-)?]
        dev-python/shtab[python_abis:*(-)?]
        dev-python/Sphinx[>=3.0][python_abis:*(-)?]
        dev-python/versioningit[python_abis:*(-)?]
        dev-python/wheel[python_abis:*(-)?]
    build+run:
        dev-python/isodate[python_abis:*(-)?]
        dev-python/lxml[>=4.6.4&<5][python_abis:*(-)?]
        dev-python/pycountry[python_abis:*(-)?]
        dev-python/pycryptodome[>=3.4.3&<4][python_abis:*(-)?]
        dev-python/PySocks[>=1.5.8][python_abis:*(-)?]
        dev-python/requests[>=2.26.0&<3.0][python_abis:*(-)?]
        dev-python/websocket-client[>=1.2.1&<2][python_abis:*(-)?]
    test:
        dev-python/freezegun[>=1.0.0][python_abis:*(-)?]
        dev-python/mock[python_abis:*(-)?]
        dev-python/requests-mock[python_abis:*(-)?]
    suggestion:
        media/ffmpeg [[
            description = [ Required for YouTube 1080p+ ]
        ]]
        media-video/rtmpdump[~scm] [[
            description = [ Required for playing RTMP based streams ]
        ]]
"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}index.html#user-guide"
UPSTREAM_CHANGELOG="${HOMEPAGE}changelog.html"

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    edo sed \
        -e "s|python |PYTHONPATH=build/lib ${PYTHON} |" \
        -i script/build-shell-completions.sh

    edo sed \
        -e "s|\$(SPHINXBUILD) -b man|PYTHONPATH=../build/lib \$(SPHINXBUILD) -b man|" \
        -i docs/Makefile

    # Not a git snapshot, use the correct version instead of 0.0.0+unknown
    edo sed \
        -e "s/default-version =.*/default-version = \"${PV}\"/" \
        -i pyproject.toml
}

install_one_multibuild() {
    # Generate the the man page...
    emake SPHINXOPTS="" --directory=docs man

    setup-py_install_one_multibuild

    # ...and shell completions.
    edo ./script/build-shell-completions.sh

    dobashcompletion completions/bash/streamlink
    dozshcompletion completions/zsh/_streamlink

    # Remove empty dirs
    option bash-completion || edo rmdir "${IMAGE}"/usr/share/bash-completion/{completions,}
    option zsh-completion || edo rmdir "${IMAGE}"//usr/share/zsh/{site-functions,}
}

