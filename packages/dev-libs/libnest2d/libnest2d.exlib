# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=tamasmeszaros ] cmake

SUMMARY="2D irregular bin packaging and nesting library written in modern C++"
DESCRIPTION="
It's a library and framework for the 2D bin packaging problem. Inspired from
the SVGNest Javascript library the project is built from scratch in C++11.
The library is written with a policy that it should be usable out of the box
with a very simple interface but has to be customizable to the very core as
well. The algorithms are defined in a header only fashion with templated
geometry types. These geometries can have custom or already existing
implementation to avoid copying or having unnecessary dependencies."

LICENCES="LGPL-3"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/boost[>=1.58]
        dev-libs/clipper[>=6.1]
        sci-libs/nlopt[>=1.4]
    test:
        dev-cpp/catch[>=2.9.1]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DLIBNEST2D_BUILD_EXAMPLES:BOOL=FLASE
)
CMAKE_SRC_CONFIGURE_TESTS+=(
    '-DLIBNEST2D_BUILD_UNITTESTS:BOOL=TRUE -DLIBNEST2D_BUILD_UNITTESTS:BOOL=FALSE'
)

