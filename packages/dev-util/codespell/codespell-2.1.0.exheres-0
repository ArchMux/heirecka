# Copyright 2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=codespell-project tag=v${PV} ]
require setup-py [ blacklist=2 import=setuptools ]
require utf8-locale

SUMMARY="Checks code for common misspellings"
DESCRIPTION="
It's designed primarily for checking misspelled words in source code, but it
can be used with other files as well."

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-python/aspell-python[python_abis:*(-)?]
        dev-python/flake8[python_abis:*(-)?]
    suggestion:
        dev-python/chardet[python_abis:*(-)?] [[
            description = [ Detect the encoding of files ]
        ]]
"

test_one_multibuild() {
    require_utf8_locale

    # "check" would pull in check-manifest as well, which probably doesn't
    # make that much sense in a tarball and fails because it misses version
    # control data.
    PATH="$(echo $(pwd)/bin):${PATH}" PYTHONPATH=$(echo $(pwd)/build/lib*) \
        emake check-dictionaries check-distutils flake8 pytest
}

